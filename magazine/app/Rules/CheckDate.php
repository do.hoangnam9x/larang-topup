<?php

namespace App\Rules;

use App\Models\Semester;
use Illuminate\Contracts\Validation\Rule;

class CheckDate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $sem = Semester:: where('end_date', '>=', $value)->first();
        return $sem == null;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.semester_startDateError');
    }
}
