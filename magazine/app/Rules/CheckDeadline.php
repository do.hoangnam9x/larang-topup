<?php

namespace App\Rules;

use App\Models\Semester;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CheckDeadline implements Rule
{

    private $request;
    /**
     * Create a new rule instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $sem = Semester::with('faculty_semester')
            ->whereHas('faculty_semester', function (Builder $builder){
                $builder->where('faculty_id', $this->request->get('faculty_id'))
                    ->where('semester_id', $this->request->get('semester_id'));
            })
            ->where('start_date', '<=', $value)
            ->where('end_date', '>=', $value)
            ->first();
        return $sem != null;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The date range of semester for deadline of this faculty is incorrect .';
    }
}
