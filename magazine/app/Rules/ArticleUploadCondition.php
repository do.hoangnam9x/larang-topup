<?php

namespace App\Rules;

use App\Models\FacultySemester;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class ArticleUploadCondition implements Rule
{
    private $typeInteraction;

    /**
     * Create a new rule instance.
     *
     * @param string $type
     */
    public function __construct($type = "update")
    {
        $this->typeInteraction = $type;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $findArticle = FacultySemester::with("semester")
            ->where('id', $value)
        ->whereHas("faculty_semester_student", function ($query) {
            $query->where("student_id", Auth::guard(STUDENT_GUARD)->user()->id);
        });
        if ($this->typeInteraction == "update")
            $findArticle->where('second_deadline', ">=", Carbon::now()->toDateTimeString());
        elseif($this->typeInteraction == "new")
            $findArticle->where('first_deadline', ">=", Carbon::now()->toDateTimeString());
        return $findArticle->first() != null;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The submission was overdue!';
    }
}
