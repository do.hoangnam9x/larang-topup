<?php

namespace App\Rules;

use App\Models\Guest;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class CheckGuestEmailSelf implements Rule
{
    private $request;

    /**
     * Create a new rule instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $existedRecord = Guest::with("faculty")
            ->where("email", $value)
            ->whereKeyNot($this->request->get("guest_id"))
            ->first();

        return !($existedRecord == true);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The email was already existed!';
    }
}
