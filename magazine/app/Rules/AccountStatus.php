<?php

namespace App\Rules;

use App\Models\Coordinator;
use App\Models\Guest;
use App\Models\Student;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class AccountStatus implements Rule
{
    private $request;
    private $userModel;

    /**
     * Create a new rule instance.
     *
     * @param Request $request
     * @param string $userModel
     */
    public function __construct(Request $request, string $userModel)
    {
        $this->request = $request;
        $this->userModel = $userModel;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->userModel == COORDINATOR_GUARD)
            $user = Coordinator::with("faculty_semester_coordinator")
                ->where('email', $value)->first();
        else if ($this->userModel == STUDENT_GUARD)
            $user = Student::with("faculty_semester_student")
                ->where('email', $value)->first();
        else if ($this->userModel == GUEST_GUARD)
            $user = Guest::with("faculty")
                ->where('email', $value)->first();
        else
            return false;
        if ($user){
            if ($user->status == ACCOUNT_DEACTIVATED) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __("auth.statusDeactivated");
    }
}
