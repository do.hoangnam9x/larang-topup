<?php

namespace App\Rules;

use App\Helpers\UploadFileValidate;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\UploadedFile;

class AttachmentFile implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param UploadedFile $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value) {
            $validEXT = UploadFileValidate::checkExtension($value->getClientOriginalExtension());
            $validMIME = UploadFileValidate::checkMime($value->getClientMimeType());
            $sizeValid = $value->getSize() < FILE_MAXSIZE;
            $validatedImage = $validEXT && $validMIME && $sizeValid;

            return $validatedImage;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Unsupported file. Please upload supported file (.docx, .jpeg, .zip)';
    }
}
