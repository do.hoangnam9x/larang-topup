<?php

namespace App\Http\Controllers;

use App\Mail\InformGrading;
use App\Models\Coordinator;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    /**
     * @param string $addressSend
     * @param Coordinator $coordinator
     * @param integer $faculty_id
     * @param integer $semester_id
     * @param string $title
     */
    public function sendGradingEmail($addressSend,
                                     $coordinator,
                                     $faculty_id,
                                     $semester_id,
                                     $title = 'Faculty submission update')
    {
        Mail::send(new InformGrading($addressSend, $coordinator, $title, $faculty_id, $semester_id));
    }
}
