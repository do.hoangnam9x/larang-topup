<?php

namespace App\Http\Requests;

use App\Rules\CheckGuestEmailSelf;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateGuestByAdmin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard(GUEST_GUARD)->check() || Auth::guard(ADMIN_GUARD)->check();

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'status' => 'required|integer|between:0,1',
            "guest_id" => 'required|exists:guests,id',
            'password' => 'nullable|min:6',
            "email" => ['required', 'email', new CheckGuestEmailSelf($this)]
        ];
        return $rule;
    }
}
