<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;


class CreateFaculty extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard(ADMIN_GUARD)->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


         return [
               'name' => 'required|regex:/^[a-zA-Z\s]*$/u|min:2|unique:faculties,name|bail',
         ];
    }

    public function messages()
    {
         return [
             'name.required' =>  'Please input Faculty name',
             'name.min' => 'Faculty name must contain at least 2 characters',

         ];
    }
}
