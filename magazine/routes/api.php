<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
      return $request->user();
  });
Route::post('new-semester','Admin\SemesterController@createSemester');

Route::post('search-faculty/{semester}/{request}','Admin\FacultyController@searchFaculty');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('search/{request}','Student\StudentController@search');
Route::get('searchAll','Student\StudentController@searchAll');

Route::get('searches/{request}','Coordinator\CoordinatorController@search');

Route::post('faculty/add-semester-faculty/{semester}/{faculty}', 'Admin\FacultyController@addSemesterFaculty_post')->name('admin.addSemesterFaculty');

