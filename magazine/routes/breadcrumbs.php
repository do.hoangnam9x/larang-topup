<?php

// Home
use App\Models\Publish;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Str;

Breadcrumbs::for('dashboard', function ($trail, $routeActive) {
    $trail->push('Dashboard', $routeActive);
});

Breadcrumbs::for('setting', function ($trail, $routeActive) {
    $trail->push('Setting', $routeActive);
});


/**
 * ## Student ##
 */
// Dashboard > Student
Breadcrumbs::for('dashboard.student', function ($trail, $parentRoute, $routeActive) {
    $trail->parent('dashboard', $parentRoute);
    $trail->push("Student", $routeActive);
});

// Dashboard > Student > create
Breadcrumbs::for('dashboard.student.create', function ($trail, $parentRoute, $parent2Route, $routeActive) {
    $trail->parent('dashboard.student', $parentRoute, $parent2Route);
    $trail->push('Create new', $routeActive);
});

// Dashboard > Student > [Student name]
Breadcrumbs::for('dashboard.student.info', function ($trail, $parentRoute, $parent2Route, $student, $routeActive) {
    $trail->parent('dashboard.student', $parentRoute, $parent2Route);
    $trail->push($student->first_name . ' ' . $student->last_name, $routeActive);
});


/**
 * ## Coordinator ##
 */
// Dashboard > Coordinator
Breadcrumbs::for('dashboard.coordinator', function ($trail, $parentRoute, $routeActive) {
    $trail->parent('dashboard', $parentRoute);
    $trail->push("Coordinator", $routeActive);
});

// Dashboard > Coordinator > create
Breadcrumbs::for('dashboard.coordinator.create', function ($trail, $parentRoute, $parent2Route, $routeActive) {
    $trail->parent('dashboard.coordinator', $parentRoute, $parent2Route);
    $trail->push("Create new", $routeActive);
});

// Dashboard > Coordinator > [Coordinator name]
Breadcrumbs::for('dashboard.coordinator.info', function ($trail, $parentRoute, $parent2Route, $coordinator, $routeActive) {
    $trail->parent('dashboard.coordinator', $parentRoute, $parent2Route);
    $trail->push($coordinator->first_name . ' ' . $coordinator->last_name, $routeActive);
});


/**
 * ## Semester ##
 */
// Dashboard > Semester
Breadcrumbs::for('dashboard.semester', function ($trail, $parentRoute, $routeActive) {
    $trail->parent('dashboard', $parentRoute);
    $trail->push("Semester", $routeActive);
});

// Dashboard > Semester > Create
Breadcrumbs::for('dashboard.semester.create', function ($trail, $parentRoute, $parent2Route, $routeActive) {
    $trail->parent('dashboard.semester', $parentRoute, $parent2Route);
    $trail->push('Create new', $routeActive);
});

// Dashboard > Semester > [Semester name]
Breadcrumbs::for('dashboard.semester.info', function ($trail, $parentRoute, $parent2Route, $semester, $routeActive) {
    $trail->parent('dashboard.semester', $parentRoute, $parent2Route);
    $trail->push($semester->name, $routeActive);
});


/**
 * ## Faculty ##
 */
// Dashboard > Faculty
Breadcrumbs::for('dashboard.faculty', function ($trail, $parentRoute, $routeActive) {
    $trail->parent('dashboard', $parentRoute);
    $trail->push("Faculty", $routeActive);
});

// Dashboard > Faculty > Detail
Breadcrumbs::for('dashboard.faculty.detail', function ($trail, $parentRoute, $parentRoute2, $routeActive) {
    $trail->parent('dashboard.faculty', $parentRoute, $parentRoute2);
    $trail->push("Detail", $routeActive);
});

// Dashboard > Faculty > Detail > New Publish
Breadcrumbs::for('dashboard.faculty.detail.newPublish', function ($trail, $parentRoute, $parentRoute2, $parentRoute3, $routeActive) {
    $trail->parent('dashboard.faculty.detail', $parentRoute, $parentRoute2, $parentRoute3);
    $trail->push("New Publish", $routeActive);
});

// Dashboard > Faculty > Detail > New Publish
Breadcrumbs::for('dashboard.faculty.detail.discussion', function ($trail, $parentRoute, $parentRoute2, $parentRoute3, $article, $routeActive) {
    $trail->parent('dashboard.faculty.detail', $parentRoute, $parentRoute2, $parentRoute3);
    $trail->push("Discussion - " . $article->student->first_name . ' ' . $article->student->last_name, $routeActive);
});

// Dashboard > Faculty > create
Breadcrumbs::for('dashboard.faculty.create', function ($trail, $parentRoute, $parent2Route, $routeActive) {
    $trail->parent('dashboard.faculty', $parentRoute, $parent2Route);
    $trail->push("Create new", $routeActive);
});

// Dashboard > Faculty > adding Student
Breadcrumbs::for('dashboard.faculty.addStudent', function ($trail, $parentRoute, $parent2Route, $routeActive) {
    $trail->parent('dashboard.faculty', $parentRoute, $parent2Route);
    $trail->push("Add Student", $routeActive);
});


/**
 * ETC
 */
// Dashboard > Faculty
Breadcrumbs::for('dashboard.profile', function ($trail, $parentRoute, $routeActive) {
    $trail->parent('dashboard', $parentRoute);
    $trail->push('Profile', $routeActive);
});

//Publishes
Breadcrumbs::for('publishes', function ($trail, $routeActive) {
    $trail->push('Publications', $routeActive);
});

//Publishes > publish
Breadcrumbs::for('publishes.publication', function ($trail, $parentRoute, $routeActive, Publish $publish) {
    $trail->parent('publishes', $parentRoute);
    $trail->push(Str::limit($publish->title, 50), $routeActive);
});

/**
 * Guest
 */
//Dashboard > Guest > Create Guest
Breadcrumbs::for('dashboard.guest.create', function ($trail, $parentRoute, $parent2Route, $routeActive) {
    $trail->parent('dashboard.guest', $parentRoute, $parent2Route);
    $trail->push("Create new", $routeActive);
});

// Dashboard > Guest
Breadcrumbs::for('dashboard.guest', function ($trail, $parentRoute, $routeActive) {
    $trail->parent('dashboard', $parentRoute);
    $trail->push("Guest", $routeActive);
});
