<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PublishingSolution extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("publish_contents", function (Blueprint  $blueprint){
            $blueprint->dropForeign("publish_contents_publish_id_foreign");
        });

        Schema::drop("publish_contents");

        Schema::table("publishes", function (Blueprint $blueprint){
            $blueprint->longText("content");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
