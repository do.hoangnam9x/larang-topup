<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PublishModified extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("publish_contents", function (Blueprint $blueprint){
            $blueprint->mediumText("content")->nullable()->change();
            $blueprint->string("image_path")->nullable()->change();
            $blueprint->mediumText("image_description")->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
