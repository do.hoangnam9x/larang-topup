import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppComponent} from './app.component';
import {DashboardComponent} from './modules/shared/dashboard/dashboard.component';
import {LoginComponent} from './modules/shared/login/login.component';
import {GuestComponent} from './layout/guest/guest.component';
import {ViewArticleComponent} from './modules/shared/view-article/view-article.component';

const routes: Routes = [
    {
        path: '', component: AppComponent, children: [
            {
                path: '', component: GuestComponent, children: [
                    {path: '', pathMatch: 'full', redirectTo: 'news'},
                    {path: 'news', component: DashboardComponent},
                    {path: 'login', component: LoginComponent},
                    {path: 'view-article', component: ViewArticleComponent},
                    //    Route for Student
                    {
                        path: 'student',
                        loadChildren: './modules/student/student.module#StudentModule'
                    },
                    //    Route for Admin
                    {
                        path: 'admin',
                        loadChildren: './modules/admin/admin.module#AdminModule'
                    },
                    //    Route for Coordinator
                    {
                        path: 'coordinator',
                        loadChildren: './modules/coordinator/coordinator.module#CoordinatorModule'
                    },
                ]
            },

            //    Route for Coordinator
            //    Route for Coordinator Master

        ]
    },
    // Test route
    // {path: 'home', component: HomeComponent},
    // {path: 'user-profile', component: ProfileComponent},
    // {path: 'signup', component: SignupComponent},
    // {path: 'landing', component: LandingComponent},
    // {path: 'nucleoicons', component: NucleoiconsComponent},
    // {path: '', redirectTo: 'home', pathMatch: 'full'}
    {path: '**', redirectTo: 'home'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
